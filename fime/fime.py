# ---------------------------------------
version = 'v1.0.0'
last_edit = '27 Jun 2020'
# ---------------------------------------

import datetime as dt
import fnmatch
import os
import sys
import time
from pathlib import Path

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from matplotlib import dates

import file_types as file_types

pd.set_option('display.max_columns', 15)
pd.set_option('display.max_rows', 100)
pd.set_option('display.width', 1000)


class FileMerger():
    def __init__(self, settings_dict, indir=False, outdir=False, logging=True):
        self.run_id, self.script_start_time = self.generate_run_id()
        self.settings_dict = settings_dict
        self.logging = logging

        # Set source folder
        # Automatic detection of working directory, where the main py resides
        abspath = os.path.abspath(__file__)
        working_directory = os.path.dirname(abspath)
        self.indir = os.path.join(working_directory, 'input_files') if not indir else indir
        self.outdir = os.path.join(working_directory, 'output') if not outdir else outdir
        if not os.path.isdir(self.outdir):
            os.makedirs(self.outdir)

        self.run()

    def print_startup_info(self):
        print(f"FIME - File Merger")
        print(f"==================")
        print(f"")
        print(f"Version: {version}")
        print(f"Last edit: {last_edit}")
        print(f"Script start time: {self.script_start_time}")
        print(f"Input folder: {self.indir}")
        print(f"Output folder: {self.outdir}")
        print(f"")
        print("Found settings:")
        for setting in self.settings_dict:
            print('    {} = {}'.format(setting, self.settings_dict[setting]))

    def run(self):

        # Logging: console txt to logfile txt output
        logfile_out = self.console_to_txt() if self.logging else -9999

        # Startup info
        print(self.print_startup_info())

        # Folder setup
        outdirs = self.setup_output_dirs(outdir=self.outdir)

        # Search files
        self.found_files = self.search_available(dir=self.indir,
                                                 pattern=self.settings_dict['FILE_ID'])

        # Merge files
        self.merged_df = self.merge()

        # Sanitize merged data
        self.merged_df = self.sanitize(df=self.merged_df)

        # Save merged data
        self.save_to_file(df=self.merged_df,
                          outdir=outdirs['merged'])

        # Plot merged data
        self.plot_merged_columns(outdir=outdirs['plots'],
                                 df=self.merged_df)

        # if self.logging:
        #     logfile_out.close()

    def insert_datetime_cols(self, df):
        if type(df.columns[0]) is tuple:
            datetime_col = ('TIMESTAMP', '[yyyy-mm-dd HH:MM:SS]')
            date_col = ('DATE', '[yyyy-mm-dd]')
            time_col = ('TIME', '[HH:MM:SS]')
        else:
            datetime_col = 'TIMESTAMP'
            date_col = 'DATE'
            time_col = 'TIME'

        df.index.name = datetime_col

        # Make sure the index is datetime
        df.index = pd.to_datetime(df.index)
        df.insert(0, time_col, df.index.time)
        df.insert(0, date_col, df.index.date)
        df.insert(0, datetime_col, df.index)
        return df

    def save_to_file(self, df, outdir):
        _df = df.copy()
        _df = self.insert_datetime_cols(df=_df)

        outname = f"{self.found_files[-1].stem}_{self.run_id}.csv"

        outfile = Path(outdir) / outname
        _df.to_csv(outfile, index=False)

    def sanitize(self, df):

        print(f"\n\n\nSANITIZING DATA\n"
              f"---------------\n")

        # print(f"    - convert all values to numeric")
        # df = df.apply(pd.to_numeric, errors='coerce')  # convert all columns in df to numeric

        if self.settings_dict['DATA_REMOVE_EMPTY_COLUMNS']:
            print(f"    - remove columns that only contain missing values")
            # Find the columns where each value is null
            empty_cols = [col for col in df.columns if df[col].isnull().all()]
            # Drop these columns from the dataframe
            df.drop(empty_cols,
                    axis=1,
                    inplace=True)
            print(f"        > removed: {empty_cols}")

        print(f"    - replace missing values with -9999")
        df = df.fillna(-9999)

        print(f"    - drop duplicates from index (keep=last)")
        df.index = df.index.drop_duplicates(keep='last')

        print(f"    - sort index ascending")
        df = df.sort_index(axis=0, ascending=True)

        print(f"    - generate continuous timestamp between first and last records")
        start = df.index[0]
        end = df.index[-1]
        print('        > generating continuous datetime timestamp from {} until {}'.format(start, end))
        print('        > data rows before date filling: {}'.format(len(df)))

        # Generate continuous date range and re-index data
        filled_date_range = pd.date_range(start, end, freq=self.settings_dict['DATA_FREQUENCY'])
        df = df.reindex(filled_date_range, fill_value=-9999)  # apply new continuous index to data
        print('        > data rows after date filling: {}'.format(len(df)))

        if self.settings_dict['DATA_MAKE_YEARLY_TIMERANGE']:
            print(f"    - make yearly time range")
            df = self.make_yearly_timerange(df=df)

        if self.settings_dict['TIMESTAMP_INDEX_COLUMN_ORIGINAL_FORMAT']:
            print(f"    - fill missing dates in original time stamp columns")
            print(f"        > filling {self.settings_dict['TIMESTAMP_PARSE_FROM_INDEX_COLUMN']}"
                  f"with formats {self.settings_dict['TIMESTAMP_INDEX_COLUMN_ORIGINAL_FORMAT']}")
            df = self.fill_orig_datecols(df=df)

        df = df.fillna(-9999)
        return df

    def fill_orig_datecols(self, df):
        """
        Fill missing values in the original date and time columns with info from
        the timestamp.

        Parameters
        ----------
        df

        Returns
        -------

        """
        _df = df.copy()
        _df.replace(-9999, np.nan, inplace=True)
        for idx, datecol in enumerate(self.settings_dict['TIMESTAMP_PARSE_FROM_INDEX_COLUMN']):
            _df['_temp'] = _df.index.strftime(self.settings_dict['TIMESTAMP_INDEX_COLUMN_ORIGINAL_FORMAT'][idx])
            _df[datecol].fillna(_df['_temp'], inplace=True)
            _df.drop(['_temp'], axis=1, inplace=True)
        _df.fillna(-9999, inplace=True)
        return _df

    def make_yearly_timerange(self, df):
        middle_date = len(df.index)
        middle_date = int(middle_date / 2)
        most_likely_year = pd.to_datetime(df.index[middle_date]).year
        most_likely_year_first_date = pd.to_datetime(dt.datetime(most_likely_year, 1, 1, 0, 30))
        most_likely_year_last_date = pd.to_datetime(dt.datetime(most_likely_year + 1, 1, 1, 0, 0))

        print(f"        > most likely year: {most_likely_year}")
        print(f"        > only using values between {most_likely_year_first_date} until {most_likely_year_last_date}")
        df = df[most_likely_year_first_date:most_likely_year_last_date]

        return df

    def merge(self):
        print(f"\n\n\nMERGING DATA\n"
              f"------------\n")
        merged_df = pd.DataFrame()
        num_files = len(self.found_files)
        all_cols = []
        order_cols = []

        for idx, f in enumerate(self.found_files):

            print(f"    Reading file {f.name} (#{idx + 1} of {num_files})")

            # Check number of header columns vs. data record columns
            more_data_cols_than_header_cols, num_missing_header_cols, header_cols_list, generated_missing_header_cols_list = \
                self.compare_len_header_vs_data(filepath=f,
                                                skip_rows_list=self.settings_dict['DATA_SKIP_ROWS'],
                                                header_rows_list=self.settings_dict['DATA_HEADER_ROWS'])
            # for k, v in settings_dict.items():
            #     print(f"{k}: {v}")

            # Read data file
            # --------------
            # Column settings for parsing dates / times correctly
            parsed_index_col = ('index', '[parsed]')
            parse_dates = self.settings_dict['TIMESTAMP_PARSE_FROM_INDEX_COLUMN']
            parse_dates = {parsed_index_col: parse_dates}
            date_parser = lambda x: dt.datetime.strptime(x, self.settings_dict['TIMESTAMP_PARSE_FROM_INDEX_COLUMN_FORMAT'])

            inc_df = pd.read_csv(f,
                                 skiprows=self.settings_dict['DATA_HEADER_SECTION_ROWS'],
                                 header=None,
                                 names=header_cols_list,
                                 na_values=self.settings_dict['DATA_NA_VALUES'],
                                 encoding='utf-8',
                                 delimiter=self.settings_dict['DATA_DELIMITER'],
                                 mangle_dupe_cols=True,
                                 keep_date_col=True,
                                 parse_dates=parse_dates,
                                 date_parser=date_parser,
                                 index_col=None,
                                 dtype=None,
                                 engine='python')

            inc_df = self.set_col_as_index(df=inc_df, settings_dict=self.settings_dict)

            if idx == 0:
                merged_df = inc_df
                all_cols += merged_df.columns.to_list()  # List of columns
                order_cols = all_cols
            else:
                prev_all_cols = all_cols
                try:
                    merged_df = merged_df.combine_first(inc_df)
                except ValueError:
                    # In case there are no overlapping column names, combine_first generates
                    # an error. In this case we use outer join instead.
                    merged_df = merged_df.join(inc_df, how='outer')

                all_cols = merged_df.columns.to_list()
                new_cols = [x for x in all_cols if x not in prev_all_cols]

                # Check header
                # Tuple means that there is more than one header row.
                if type(new_cols[0]) is tuple:
                    for new_col, new_units in new_cols:
                        print(f"        + Adding new column: {new_col} {new_units}")
                else:
                    for new_col in new_cols:
                        print(f"        + Adding new column: {new_col}")

                order_cols += new_cols

        # Column order close to original, with newest added columns at the end
        merged_df = merged_df[order_cols]

        return merged_df

    def compare_len_header_vs_data(self, filepath, skip_rows_list, header_rows_list):
        """
        Check whether there are more data columns than given in the header.

        If not checked, this would results in an error when reading the csv file
        with .read_csv, because the method expects an equal number of header and
        data columns. If this check is True, then the difference between the length
        of the first data row and the length of the header row(s) can be used to
        automatically generate names for the missing header columns.
        """
        # Check number of columns of the first data row after the header part
        skip_num_lines = len(header_rows_list) + len(skip_rows_list)
        first_data_row_df = pd.read_csv(filepath, skiprows=skip_num_lines,
                                        header=None, nrows=1)
        len_data_cols = first_data_row_df.columns.size

        # Check number of columns of the header part
        header_cols_df = pd.read_csv(filepath, skiprows=skip_rows_list,
                                     header=header_rows_list, nrows=0)
        len_header_cols = header_cols_df.columns.size

        # Check if there are more data columns than header columns
        if len_data_cols > len_header_cols:
            more_data_cols_than_header_cols = True
            num_missing_header_cols = len_data_cols - len_header_cols
        else:
            more_data_cols_than_header_cols = False
            num_missing_header_cols = 0

        # Generate missing header columns if necessary
        header_cols_list = header_cols_df.columns.to_list()
        generated_missing_header_cols_list = []
        sfx = self.make_timestamp_microsec_suffix()
        if more_data_cols_than_header_cols:
            for m in list(range(1, num_missing_header_cols + 1)):
                missing_col = (f'unknown_{m}-{sfx}', '[-unknown-]')
                generated_missing_header_cols_list.append(missing_col)
                header_cols_list.append(missing_col)

        print(f"        > File has more header columns than data columns: {more_data_cols_than_header_cols}")
        print(f"        > Number of missing header columns: {num_missing_header_cols}")
        print(f"        > Generated missing header columns: {generated_missing_header_cols_list}")

        return more_data_cols_than_header_cols, num_missing_header_cols, header_cols_list, generated_missing_header_cols_list

    def make_timestamp_microsec_suffix(self):
        now_time_dt = dt.datetime.now()
        now_time_str = now_time_dt.strftime("%H%M%S%f")
        run_id = f'{now_time_str}'
        # log(name=make_run_id.__name__, dict={'run id': run_id}, highlight=False)
        return run_id

    @staticmethod
    def search_available(dir, pattern):
        """Search files in dir.

        :param dir: Directory that is searched
        :param pattern: Pattern to identify files
        :return: List of found files
        :rtype: list
        """

        found_files = []
        for root, dirs, files in os.walk(dir):
            root = Path(root)
            for idx, filename in enumerate(files):
                if fnmatch.fnmatch(filename, pattern):
                    filepath = Path(root) / Path(filename)
                    found_files.append(filepath)
        found_files.sort()  # Sorts inplace
        return found_files

    def set_col_as_index(self, df, settings_dict):
        # Index name is now the same for all filetypes w/ timestamp in data
        df.set_index([('index', '[parsed]')], inplace=True)
        df.index = pd.to_datetime(df.index)
        return df

    def plot_merged_columns(self, outdir, df):
        print('\n\n\nPLOTTING DATA COLUMNS')
        print('---------------------\n\n')
        print('     time resolution: {}'.format(df.index.freqstr))

        df = df.replace(-9999, np.nan)
        num_cols = len(df.columns)
        start = df.index[0]
        end = df.index[-1]
        x = df.index.to_pydatetime()

        for idx, col in enumerate(df.columns):
            y = df[col]

            # Check values
            len_y = len(y.dropna())
            if len_y > 0:
                print(f"     plotting {col} (#{idx + 1} of {num_cols}, {len_y} records)")
            else:
                print(f"       skipping {col} (#{idx + 1} of {num_cols})  --> {len_y} records")
                continue

            fig = plt.figure(figsize=(16, 10))
            gs = fig.add_gridspec(1, 1)
            gs.update(wspace=0, hspace=0)  # set the spacing between axes.
            ax1 = fig.add_subplot(gs[0, 0])
            ax1.set_title(f"{col}")

            if np.issubdtype(df[col].dtype, np.number):
                lns1 = ax1.plot_date(x=x, y=y,
                                     c='#ef5350', markeredgecolor='none',
                                     alpha=1, markersize=4,
                                     zorder=98, label=col)
                ax1.set_xlabel("DATE", size=8)
                ax1.tick_params(axis='both', labelsize=8)
                ax1.xaxis.set_major_formatter(dates.DateFormatter("%y-%m-%d"))
                ax1.set_xlim(start, end)

                if (y.min() < 0) and (y.max() > 0):
                    ax1.axhline(0, color='black', alpha=1, linewidth=1)
                    ax1.set_ylim(y.quantile(0.01), y.quantile(0.99))
                # elif (y.min() > 0) and (y.max() > 0):
                #     ax1.set_ylim(y.min(), y.quantile(0.99))
                # else:
                #     ax1.set_ylim(y.quantile(0.01), y.max())

                labels = [l.get_label() for l in lns1]
                ax1.legend(lns1, labels, loc=0, fontsize=5)
                ax1.tick_params(axis='y', which='major', bottom='on', labelsize=8)
                # ax1.text(0.01, 0.95, outname, transform=ax1.transAxes, ha='left', zorder=99, size=16)

                txt_info = f"first date: {x[0]}\n" \
                           f"last date: {x[-1]}\n" \
                           f"values: {len(y)}\n" \
                           f"median: {y.median():.3f}\n" \
                           f"IQR: {y.quantile(0.25):.3f} to {y.quantile(0.75):.3f}\n" \
                           f"range: {y.min()}, {y.max()}\n" \
                           f"only data between 1st percentile {y.quantile(0.01):.3f} and " \
                           f"99th percentile {y.quantile(0.99):.3f} are shown"

                ax1.text(0.05, 0.95, txt_info, horizontalalignment='left',
                         verticalalignment='top', transform=ax1.transAxes, backgroundcolor='None',
                         size=10, zorder=99)
            else:
                ax1.text(0.5, 0.5, f"- no numeric data -", horizontalalignment='center',
                         verticalalignment='center', transform=ax1.transAxes, backgroundcolor='#b1d32f',
                         size=20, zorder=99)

            outname = self.convert_tuple_string_to_single_line(col)
            outfile = os.path.join(outdir, '{}.png'.format(outname))
            plt.savefig(outfile, dpi=100)
            plt.close()

            # except ValueError as e:
            #     print("Skipped ({})".format(e))

    def convert_tuple_string_to_single_line(self, tuple_string):
        if isinstance(tuple_string,
                      tuple):  # needed to make nice looking one-line header b/c multirow header makes y_name a tuple which is ugly to output on plots and in filenames
            single_line_string = str(tuple_string)  # make string
            intab = b",'()?:%[]"  # replace these characters ...
            outtab = b"_        "  # ... with these characters
            trantab = bytes.maketrans(intab, outtab)  # necessary for translate
            single_line_string = single_line_string.translate(trantab)  # replacing the characters
            single_line_string = single_line_string.replace(" ", "")  # remove empty space
            single_line_string = single_line_string.replace("_mean", "")  # remove empty space
            single_line_string = single_line_string.replace("/", "_over_")  # remove empty space
            single_line_string = single_line_string.replace("*", "star")  # remove empty space
        else:
            single_line_string = tuple_string

        return single_line_string

    def setup_output_dirs(self, outdir='output', del_previous_results=True):
        """Make output directories."""
        new_dirs = ['merged', 'plots']
        outdirs = {}

        # Store keys and full paths in dict
        for nd in new_dirs:
            # outdirs[nd] = Path(outdir)
            outdirs[nd] = outdir / Path(nd)

        # Make dirs
        for key, path in outdirs.items():
            if not Path.is_dir(path):
                print(f"Creating folder {path} ...")
                os.makedirs(path)
            else:
                if del_previous_results:
                    for filename in os.listdir(path):
                        filepath = os.path.join(path, filename)
                        try:
                            if os.path.isfile(filepath) or os.path.islink(filepath):
                                print(f"Deleting file {filepath} ...")
                                os.unlink(filepath)
                            # elif os.path.isdir(filepath):
                            #     shutil.rmtree(filepath)
                        except Exception as e:
                            print('Failed to delete %s. Reason: %s' % (filepath, e))

        return outdirs

    def generate_run_id(self):
        # current time
        script_start_time = time.strftime("%Y-%m-%d %H:%M:%S")
        run_id = time.strftime("%Y%m%d-%H%M%S")
        run_id = f"FIME-{run_id}"
        return run_id, script_start_time

    def console_to_txt(self):
        """
        Write all console output to logfile txt file

        Returns
        -------
        Path to log file

        """

        logfile_outdir = os.path.join(self.outdir, 'log')
        if not os.path.isdir(logfile_outdir):
            os.makedirs(logfile_outdir)

        logfile_name = f"log_{self.run_id}.txt"

        logfile_path = os.path.join(logfile_outdir, logfile_name)
        logfile_path = open(logfile_path, 'w')
        sys.stdout = Tee(sys.stdout, logfile_path)
        return logfile_path


# class to write to console AND log file with print
class Tee(object):
    def __init__(self, *files):
        self.files = files

    def write(self, obj):
        for ff in self.files:
            ff.write(obj)

    def flush(self):
        pass


if __name__ == '__main__':
    FileMerger(indir=False,
               outdir=False,
               settings_dict=file_types.eddypro_fluxnet_30T(),
               logging=True)

    print("Thank you for using this script.")
