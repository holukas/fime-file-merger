# TIMESTAMP
#   'full' = yyyy-mm-dd HH:MM; datetime TS index is already available in data file
#   'date_time_cols' = builds full TS column from the two separate columns "date" and "time" (typical EddyPro output)
#   'iso' = yyyymmddHHMMSS, e.g. GHG-europe file uses the ISO format
#   'doy' = TS given as day of year
#   'same' = pass

def eddypro_full_output_30T():
    """Settings for EddyPro's *_full_output_*.csv file."""
    settings_dict = {
        'NAME': 'eddypro_full_output_30T',
        'DESCRIPTION': 'Default EddyPro output file',
        'FILE_ID': '*_full_output_*.csv',
        'TIMESTAMP_PARSE_FROM_INDEX_COLUMN': [('date', '[yyyy-mm-dd]'), ('time', '[HH:MM]')],
        'TIMESTAMP_PARSE_FROM_INDEX_COLUMN_FORMAT': '%Y-%m-%d %H:%M',
        'TIMESTAMP_INDEX_COLUMN_ORIGINAL_FORMAT': ['%Y-%m-%d', '%H:%M'],
        # 'TIMESTAMP_DATETIME_FORMAT_EXPORT': '%Y-%m-%d %H:%M',
        'DATA_HEADER_SECTION_ROWS': [0, 1, 2],  # list; all header rows
        'DATA_SKIP_ROWS': [0],  # list; ignore these rows
        'DATA_HEADER_ROWS': [0, 1],  # list; header rows that should be used AFTER skip_rows is considered
        'DATA_NA_VALUES': -9999,
        'DATA_FREQUENCY': '30T',  # string; time resolution of data, using python pandas freq convention
        'DATA_DELIMITER': ',',
        'DATA_REMOVE_EMPTY_COLUMNS': False,
        'DATA_MAKE_YEARLY_TIMERANGE': True
    }

    # file_settings = {
    #     'FILENAME_id': '*.csv',
    #     'TIMESTAMP_input': 'date_time_cols',  # string; available TS format
    #     'TIMESTAMP_output': 'date_time_cols',  # string; output TS format
    #     'TIMESTAMP_yearly_range': False,  # bool; e.g. output between 2012-01-01 00:30 and 2013-01-01 00:00
    #     'TIMESTAMP_fill_date_gaps': True,  # bool; no gap-filling, missing dates are added with missing values -9999
    #     'TIMESTAMP_special_correction': 'none',  # string; for special cases, e.g. NABEL files
    #     'TIMESTAMP_format': 'none',  # string; for special cases, e.g. NABEL files
    #     'HEADER_remove_unit_brackets': False,  # bool; EP ghg-europe output files use brackets around units
    #     'DATA_remove_non_numeric_cols': False,  # bool; deleted from output file
    #     'DATA_resample_to': ['30T', '1W'],  # list; set -9999 for no resampling
    #     'DATA_resample_how': 'mean'
    # }

    return settings_dict


def eddypro_fluxnet_30T():
    settings_dict = {
        'NAME': 'eddypro_fluxnet_30T',
        'DESCRIPTION': 'EddyPro FLUXNET output file (formatted for FLUXNET)',
        'FILE_ID': '*_fluxnet_*.csv',
        'TIMESTAMP_PARSE_FROM_INDEX_COLUMN': ['TIMESTAMP_END'],
        'TIMESTAMP_PARSE_FROM_INDEX_COLUMN_FORMAT': '%Y%m%d%H%M',
        'TIMESTAMP_INDEX_COLUMN_ORIGINAL_FORMAT': ['%Y%m%d%H%M'],
        # 'TIMESTAMP_DATETIME_FORMAT_EXPORT': '%Y-%m-%d %H:%M',#todo needed?
        'DATA_HEADER_SECTION_ROWS': [0],
        'DATA_SKIP_ROWS': [],
        'DATA_HEADER_ROWS': [0],
        'DATA_NA_VALUES': -9999,
        'DATA_FREQUENCY': '30T',
        'DATA_DELIMITER': ',',
        'DATA_REMOVE_EMPTY_COLUMNS': False,
        'DATA_MAKE_YEARLY_TIMERANGE': True
    }

    return settings_dict


    file_settings = {}

    # FILENAME
    # eddypro_CH-DAV_ghg-europe_eddy_2019-02-24T030647_adv.csv
    file_settings['FILENAME_id'] = '*_ghg-europe_eddy_*.csv'

    # TIMESTAMP
    file_settings['TIMESTAMP_input'] = 'iso'  # string; available TS format
    file_settings['TIMESTAMP_output'] = 'date_time_cols'  # string; output TS format
    # file_settings['TIMESTAMP_output'] = 'iso'  # string; output TS format
    file_settings[
        'TIMESTAMP_yearly_range'] = True  # bool; relevant for yearly files, e.g. output all values between 2012-01-01 00:30 and 2013-01-01 00:00
    file_settings[
        'TIMESTAMP_fill_date_gaps'] = True  # bool; no gap-filling, missing dates are added with missing values -9999
    file_settings['TIMESTAMP_special_correction'] = 'none'  # string; for special cases, e.g. NABEL files

    # HEADER
    file_settings['HEADER_rows'] = [0, 1]  # list; header rows that should be used
    file_settings['HEADER_remove_unit_brackets'] = False  # bool; EP ghg-europe output files use brackets around units

    # DATA
    file_settings['DATA_remove_non_numeric_cols'] = False  # bool; deleted from output file
    file_settings['DATA_freq'] = '30T'  # string; time resolution of data, using python pandas freq convention
    file_settings['DATA_resample_to'] = [-9999]  # list; set -9999 for no resampling
    file_settings['DATA_resample_how'] = 'mean'  # string;

    return file_settings


def FILE_1HeaderRow_30T():
    file_settings = {}

    # FILENAME
    # CH-DAV_CR1000_T1_35_1_TBL1_20160101-0650.csv
    file_settings['FILENAME_id'] = '*.csv'

    # TIMESTAMP
    file_settings['TIMESTAMP_input'] = 'full'  # string; available TS format
    file_settings['TIMESTAMP_output'] = 'full'  # string; output TS format
    file_settings[
        'TIMESTAMP_yearly_range'] = False  # bool; relevant for yearly files, e.g. output all values between 2012-01-01 00:30 and 2013-01-01 00:00
    file_settings[
        'TIMESTAMP_fill_date_gaps'] = True  # bool; no gap-filling, missing dates are added with missing values -9999
    file_settings['TIMESTAMP_special_correction'] = 'none'  # string; for special cases, e.g. NABEL files

    # HEADER
    file_settings['HEADER_rows'] = [0]  # list; header rows that should be used
    file_settings['HEADER_remove_unit_brackets'] = False  # bool; EP ghg-europe output files use brackets around units

    # DATA
    file_settings['DATA_remove_non_numeric_cols'] = False  # bool; deleted from output file
    file_settings['DATA_freq'] = '30T'  # string; time resolution of data, using python pandas freq convention
    file_settings['DATA_resample_to'] = [-9999]  # list; set -9999 for no resampling
    file_settings['DATA_resample_how'] = 'mean'  # string;

    return file_settings


def DAV_10_meteo_1T():
    file_settings = {}

    # FILENAME
    # CH-DAV_CR1000_T1_35_1_TBL1_20160101-0650.csv
    file_settings['FILENAME_id'] = '*.csv'

    # TIMESTAMP
    file_settings['TIMESTAMP_input'] = 'full'  # string; available TS format
    file_settings['TIMESTAMP_output'] = 'full'  # string; output TS format
    file_settings[
        'TIMESTAMP_yearly_range'] = False  # bool; relevant for yearly files, e.g. output all values between 2012-01-01 00:30 and 2013-01-01 00:00
    file_settings[
        'TIMESTAMP_fill_date_gaps'] = True  # bool; no gap-filling, missing dates are added with missing values -9999
    file_settings['TIMESTAMP_special_correction'] = 'none'  # string; for special cases, e.g. NABEL files

    # HEADER
    file_settings['HEADER_rows'] = [0]  # list; header rows that should be used
    file_settings['HEADER_remove_unit_brackets'] = False  # bool; EP ghg-europe output files use brackets around units

    # DATA
    file_settings['DATA_remove_non_numeric_cols'] = False  # bool; deleted from output file
    file_settings['DATA_freq'] = '1T'  # string; time resolution of data, using python pandas freq convention
    file_settings['DATA_resample_to'] = ['30T', 'D']  # list; set -9999 for no resampling
    file_settings['DATA_resample_how'] = 'mean'  # string;

    return file_settings


def FINAL_FLUXES_30T():
    file_settings = {}

    # FILENAME
    file_settings['FILENAME_id'] = '*.csv'

    # TIMESTAMP
    file_settings['TIMESTAMP_input'] = 'full'  # string; available TS format
    file_settings['TIMESTAMP_output'] = 'full'  # string; output TS format
    file_settings[
        'TIMESTAMP_yearly_range'] = False  # bool; relevant for yearly files, e.g. output all values between 2012-01-01 00:30 and 2013-01-01 00:00
    file_settings[
        'TIMESTAMP_fill_date_gaps'] = True  # bool; no gap-filling, missing dates are added with missing values -9999
    file_settings['TIMESTAMP_special_correction'] = 'none'  # string; for special cases, e.g. NABEL files

    # HEADER
    file_settings['HEADER_rows'] = [0, 1]  # list; header rows that should be used
    file_settings['HEADER_remove_unit_brackets'] = False  # bool; EP ghg-europe output files use brackets around units

    # DATA
    file_settings['DATA_remove_non_numeric_cols'] = True  # bool; deleted from output file
    file_settings['DATA_freq'] = '30T'  # string; time resolution of data, using python pandas freq convention
    file_settings['DATA_resample_to'] = ['D']  # list; set -9999 for no resampling
    file_settings['DATA_resample_how'] = 'mean'  # string;

    return file_settings


def CUSTOM_NABEL_10T():
    file_settings = {}

    # FILENAME
    file_settings['FILENAME_id'] = '*.csv'

    # TIMESTAMP
    file_settings['TIMESTAMP_input'] = 'full'  # string; available TS format
    file_settings['TIMESTAMP_output'] = 'full'  # string; output TS format
    file_settings[
        'TIMESTAMP_yearly_range'] = False  # bool; relevant for yearly files, e.g. output all values between 2012-01-01 00:30 and 2013-01-01 00:00
    file_settings[
        'TIMESTAMP_fill_date_gaps'] = True  # bool; no gap-filling, missing dates are added with missing values -9999
    file_settings[
        'TIMESTAMP_special_correction'] = 'nabel'  # string; daily timestamp goes from e.g. 2016-01-01 00:10 (first) until 2016-01-01 00:00 (last)

    # HEADER
    file_settings['HEADER_rows'] = [0]  # list; header rows that should be used
    file_settings['HEADER_remove_unit_brackets'] = False  # bool; EP ghg-europe output files use brackets around units

    # DATA
    file_settings['DATA_remove_non_numeric_cols'] = True  # bool; deleted from output file
    file_settings['DATA_freq'] = '10T'  # string; time resolution of data, using python pandas freq convention
    file_settings['DATA_resample_to'] = ['30T']  # list; set -9999 for no resampling
    file_settings['DATA_resample_how'] = 'mean'  # string;

    return file_settings


def CUSTOM_30T():
    file_settings = {}

    # FILENAME
    file_settings['FILENAME_id'] = '*.csv'

    # TIMESTAMP
    file_settings['TIMESTAMP_input'] = 'date_time_cols'  # string; available TS format
    # file_settings['TIMESTAMP_input'] = 'iso'  # string; available TS format
    # file_settings['TIMESTAMP_input'] = 'full'  # string; available TS format

    # file_settings['TIMESTAMP_output'] = 'full'  # string; output TS format
    file_settings['TIMESTAMP_output'] = 'date_time_cols'  # string; output TS format

    file_settings[
        'TIMESTAMP_yearly_range'] = True  # bool; relevant for yearly files, e.g. output all values between 2012-01-01 00:30 and 2013-01-01 00:00
    file_settings[
        'TIMESTAMP_fill_date_gaps'] = True  # bool; no gap-filling, missing dates are added with missing values -9999
    file_settings[
        'TIMESTAMP_special_correction'] = 'none'  # string; daily timestamp goes from e.g. 2016-01-01 00:10 (first) until 2016-01-01 00:00 (last)

    # HEADER
    file_settings['HEADER_rows'] = [0]  # list; header rows that should be used
    # file_settings['HEADER_rows'] = [0, 1]  # list; header rows that should be used
    file_settings['HEADER_remove_unit_brackets'] = False  # bool; EP ghg-europe output files use brackets around units

    # DATA
    file_settings['DATA_remove_non_numeric_cols'] = False  # bool; deleted from output file
    file_settings['DATA_freq'] = '30T'  # string; time resolution of data, using python pandas freq convention
    file_settings['DATA_resample_to'] = [-9999]  # list; set -9999 for no resampling
    file_settings['DATA_resample_how'] = 'mean'  # string;

    return file_settings
